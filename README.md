This README file contains information on the contents of the meta-dpe-low layer.    
Please see the corresponding sections below for details.

## Dependencies
This meta-layer is used and tested with Oniro. Please see the official documentation for more details: https://docs.oniroproject.org/en/latest/overview/index.html

* URI: https://booting.oniroproject.org/distro/oniro    
* branch: dunfell    

## Patches
Please submit any patches against the meta-dpe-high layer please create a merge request for this project
* URI: https://gitlab.com/harmonyos-hackathon/resources-oniro/meta-dpe-low    
* Maintainer: Alin Popa (alin.popa.ext@huawei.com)

## Table of Contents
I. Adding the meta-dpe-low layer to your build    
II. Build a demo

### I. Adding the meta-dpe-low layer to your build
1. Please follow the instructions from local manifests      
https://gitlab.com/harmonyos-hackathon/resources-oniro/local-manifests/-/blob/main/README.md

2. Source the Zephyr flavour
```
# TEMPLATECONF=../oniro/flavours/zephyr . ./oe-core/oe-init-build-env build-oniro-zephyr
```

3. Add the layer in your layer config
```
# vim conf/bblayers.conf
# <add meta-dpe-low as any other layers from sources directory>
```

### II. Build a demo
A demo target can be built by specifying the MACHINE and adding the needed recipes to the target.    
Eg. NRF52840dk Demo: https://gitlab.com/harmonyos-hackathon/resources-oniro/nrf52840dk-demo

```
# MACHINE=nrf52840dk_nrf52840 bitbake nrf52840dk-demo
```
