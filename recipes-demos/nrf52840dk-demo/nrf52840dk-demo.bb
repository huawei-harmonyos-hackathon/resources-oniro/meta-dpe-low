APPNAME = "nrfdemo"
SUMMARY = "NRF52840-DK Demo"
DESCRIPTION = "A simple demo project using nrf52840-dk hardware"
AUTHOR = "alin.popa.ext@huawei.com"
LICENSE = "Apache-2.0"
LIC_FILES_CHKSUM = "file://${WORKDIR}/git/${APPNAME}/LICENSE;md5=7388a7144776640b5c29ecbfa8358b44"

require system-modules.inc
inherit deploy

SRC_URI += "\
    git://gitlab.com/harmonyos-hackathon/resources/nrf52840dk-demo.git;protocol=https;branch=main;destsuffix=git/${APPNAME};name=${APPNAME} \
    "
SRCREV_${APPNAME} = "${AUTOREV}"
PV = "git${SRCREV_${APPNAME}}"

ZEPHYR_SRC_DIR = "${WORKDIR}/git/${APPNAME}"
OECMAKE_SOURCEPATH = "${ZEPHYR_SRC_DIR}"

do_install[noexec] = "1"

do_deploy () {
    install -D ${B}/zephyr/${ZEPHYR_MAKE_OUTPUT} ${DEPLOYDIR}/${PN}.elf
}
addtask deploy after do_compile

COMPATIBLE_MACHINE = "(nrf52840dk-nrf52840)"
