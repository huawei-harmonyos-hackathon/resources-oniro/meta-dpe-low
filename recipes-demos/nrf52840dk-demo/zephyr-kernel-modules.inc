# Include Zephyr kernel
require recipes-kernel/zephyr-kernel/zephyr-kernel-src.inc
require recipes-kernel/zephyr-kernel/zephyr-kernel-common.inc

# LVGL module
SRC_URI += "\
    git://github.com/zephyrproject-rtos/lvgl.git;protocol=https;branch=zephyr;destsuffix=git/modules/lib/gui/lvgl;name=lvgl \
    "
SRCREV_lvgl = "31acbaa36e9e74ab88ac81e3d21e7f1d00a71136"

# SEGGER module
SRC_URI += "\
    git://github.com/zephyrproject-rtos/segger.git;protocol=https;branch=master;destsuffix=git/modules/debug/segger;name=segger \
    "
SRCREV_segger = "2aa031c081426dcd0626185af45c40bd05811b68"

ZEPHYR_EXTRA_MODULES = "\;${S}/modules/lib/gui/lvgl\;${S}/modules/debug/segger"
EXTRA_OECMAKE_append = " -DZEPHYR_EXTRA_MODULES=${ZEPHYR_EXTRA_MODULES}"
